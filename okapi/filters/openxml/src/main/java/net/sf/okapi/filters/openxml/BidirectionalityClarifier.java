/*===========================================================================
  Copyright (C) 2016-2017 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.filters.openxml;

import java.util.HashSet;
import java.util.ListIterator;
import java.util.Set;


/**
 * Provides a bidirectionality clarifier.
 */
class BidirectionalityClarifier {

    private static final int MAX_NUMBER_OF_CLARIFIABLE_WORD_STYLE_TYPES = 3;

    private final ConditionalParameters conditionalParameters;
    private final CreationalParameters creationalParameters;
    private final ClarificationParameters clarificationParameters;
    private final Set<String> clarifiableWordStyleTypes;

    /**
     * Constructs the bidirectionality clarifier.
     *
     * @param conditionalParameters   Conditional parameters
     * @param creationalParameters    Creational parameters
     * @param clarificationParameters Clarification parameters
     */
    BidirectionalityClarifier(
        ConditionalParameters conditionalParameters,
        CreationalParameters creationalParameters,
        ClarificationParameters clarificationParameters
    ) {
        this.conditionalParameters = conditionalParameters;
        this.creationalParameters = creationalParameters;
        this.clarificationParameters = clarificationParameters;
        this.clarifiableWordStyleTypes = new HashSet<>(MAX_NUMBER_OF_CLARIFIABLE_WORD_STYLE_TYPES);
    }

    /**
     * Clarifies a markup.
     */
    void clarifyMarkup(Markup markup) {
        ListIterator<MarkupComponent> iterator = markup.components().listIterator();

        while (iterator.hasNext()) {
            MarkupComponent component = iterator.next();

            if (MarkupComponent.isSheetViewStart(component)) {
                clarifySheetView(component);
            } else if (MarkupComponent.isAlignmentEmptyElement(component)) {
                clarifyAlignment(component);
            } else if (MarkupComponent.isPresentationStart(component)) {
                clarifyPresentation(component);
            } else if (MarkupComponent.isTableStart(component)) {
                clarifyTableProperties(iterator);
            } else if (MarkupComponent.isTextBodyStart(component)) {
                clarifyTextBodyProperties(iterator);
            } else if (MarkupComponent.isParagraphStart(component)) {
                clarifyParagraphProperties(iterator);
            } else if (MarkupComponent.isWordStylesStart(component)) {
                clarifyWordStyles(iterator);
            }
        }
    }

    private void clarifySheetView(MarkupComponent markupComponent) {
        new MarkupComponentClarifier.SheetViewClarifier(conditionalParameters, creationalParameters, clarificationParameters)
                .clarify(markupComponent);
    }

    private void clarifyAlignment(MarkupComponent markupComponent) {
        new MarkupComponentClarifier.AlignmentClarifier(conditionalParameters, creationalParameters, clarificationParameters)
                .clarify(markupComponent);
    }

    private void clarifyPresentation(MarkupComponent markupComponent) {
        new MarkupComponentClarifier.PresentationClarifier(conditionalParameters, creationalParameters, clarificationParameters)
                .clarify(markupComponent);
    }

    private void clarifyTableProperties(ListIterator<MarkupComponent> markupComponentIterator) {
        new BlockPropertiesClarifier.TablePropertiesClarifier(conditionalParameters, creationalParameters, clarificationParameters)
                .clarify(markupComponentIterator);
    }

    private void clarifyTextBodyProperties(ListIterator<MarkupComponent> markupComponentIterator) {
        new BlockPropertiesClarifier.TextBodyPropertiesClarifier(conditionalParameters, creationalParameters, clarificationParameters)
                .clarify(markupComponentIterator);
    }

    private void clarifyParagraphProperties(ListIterator<MarkupComponent> markupComponentIterator) {
        new BlockPropertiesClarifier.ParagraphPropertiesClarifier(conditionalParameters, creationalParameters, clarificationParameters)
                .clarify(markupComponentIterator);
    }

    private void clarifyWordStyles(final ListIterator<MarkupComponent> iterator) {
        this.clarifiableWordStyleTypes.add(StyleType.TABLE.toString());
        this.clarifiableWordStyleTypes.add(StyleType.PARAGRAPH.toString());
        this.clarifiableWordStyleTypes.add(StyleType.CHARACTER.toString());
        while (iterator.hasNext()) {
            final MarkupComponent component = iterator.next();

            if (MarkupComponent.isWordDocumentDefaultsStart(component)) {
                clarifyWordDocumentDefaults(iterator);
            } else if (MarkupComponent.isWordStyleStart(component)) {
                clarifyWordStyle((MarkupComponent.Start) component, iterator);
            } else if (MarkupComponent.isWordStylesEnd(component)) {
                break;
            }
        }
    }

    private void clarifyWordDocumentDefaults(final ListIterator<MarkupComponent> iterator) {
        while (iterator.hasNext()) {
            final MarkupComponent component = iterator.next();

            if (MarkupComponent.isWordParagraphPropertiesDefaultStart(component)) {
                clarifyParagraphProperties(iterator);
                this.clarifiableWordStyleTypes.remove(StyleType.PARAGRAPH.toString());
            } else if (MarkupComponent.isWordRunPropertiesDefaultStart(component)) {
                clarifyDefaultRunProperties(iterator);
                this.clarifiableWordStyleTypes.remove(StyleType.CHARACTER.toString());
            } else if (MarkupComponent.isWordDocumentDefaultsEnd(component)) {
                break;
            }
        }
    }

    private void clarifyDefaultRunProperties(final ListIterator<MarkupComponent> iterator) {
        while (iterator.hasNext()) {
            final MarkupComponent component = iterator.next();

            if (MarkupComponent.isRunProperties(component)) {
                iterator.set(clarifyRunProperties((RunProperties) component));
            } else if (MarkupComponent.isWordRunPropertiesDefaultEnd(component)) {
                break;
            }
        }
    }

    /**
     * Clarifies run properties.
     *
     * @param runProperties Run properties
     *
     * @return Clarified run properties
     *
     * {@code null} if the resulted run properties are empty
     */
    RunProperties clarifyRunProperties(RunProperties runProperties) {
        return new RunPropertiesClarifier(creationalParameters, clarificationParameters)
                .clarify(runProperties);
    }

    private void clarifyWordStyle(final MarkupComponent.Start startComponent, final ListIterator<MarkupComponent> iterator) {
        if (
            !startComponent.containsAttributeWithAnyOfValues(
                WordStyleDefinition.General.TYPE,
                clarifiableWordStyleTypes
            )
            || !startComponent.containsAttributeWithAnyOfValues(
                WordStyleDefinition.General.DEFAULT,
                XMLEventHelpers.booleanAttributeTrueValues()
            )
        ) {
            return;
        }
        while (iterator.hasNext()) {
            final MarkupComponent component = iterator.next();

            if (MarkupComponent.isParagraphBlockProperties(component)) {
                iterator.previous();
                clarifyParagraphProperties(iterator);
            } else if (MarkupComponent.isRunProperties(component)) {
                iterator.set(clarifyRunProperties((RunProperties) component));
            } else if (MarkupComponent.isTableBlockProperties(component)) {
                iterator.previous();
                clarifyTableProperties(iterator);
            } else if (MarkupComponent.isWordStyleEnd(component)) {
                break;
            }
        }
    }
}
