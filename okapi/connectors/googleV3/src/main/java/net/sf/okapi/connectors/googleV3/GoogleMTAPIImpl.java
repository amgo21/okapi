/*===========================================================================
  Copyright (C) 2017 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.connectors.googleV3;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import net.sf.okapi.common.Util;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.sf.okapi.common.StreamUtil;
import net.sf.okapi.common.exceptions.OkapiException;

import com.google.cloud.translate.v3.LocationName;
import com.google.cloud.translate.v3.TranslateTextRequest;
import com.google.cloud.translate.v3.TranslateTextResponse;
import com.google.cloud.translate.v3.Translation;
import com.google.cloud.translate.v3.TranslationServiceClient;
import com.google.cloud.translate.v3.GetSupportedLanguagesRequest;
import com.google.cloud.translate.v3.SupportedLanguage;
import com.google.cloud.translate.v3.SupportedLanguages;
import com.google.cloud.translate.v3.GlossaryName;
import com.google.cloud.translate.v3.TranslateTextGlossaryConfig;
import java.io.IOException;

public class GoogleMTAPIImpl implements GoogleMTAPI {
    private final Logger LOG = LoggerFactory.getLogger(getClass());
    private GoogleMTv3Parameters params;
    private GoogleResponseParser parser = new GoogleResponseParser();

    public GoogleMTAPIImpl(GoogleMTv3Parameters params) {
        this.params = params;
    }

    @Override
    public List<String> getLanguages() throws IOException, ParseException {
        try (TranslationServiceClient client = TranslationServiceClient.create()) {
            LocationName parent = LocationName.of(params.getProjectId(), "global");
            GetSupportedLanguagesRequest request =
                    GetSupportedLanguagesRequest.newBuilder().setParent(parent.toString()).build();

            SupportedLanguages response = client.getSupportedLanguages(request);
            List<String> languages = new ArrayList<>();
            for (SupportedLanguage language : response.getLanguagesList()) {
                languages.add(language.getLanguageCode());
            }
            return languages;
        }
    }

    @Override
    public <T> List<TranslationResponse> translate(GoogleQueryBuilder<T> qb) throws IOException, ParseException  {
        System.out.println("hi");
        try (TranslationServiceClient client = TranslationServiceClient.create()) {
            String projectCredentials = params.getProjectId();
            String location = params.getLocation();
            LocationName parent = LocationName.of(projectCredentials, location);
            TranslateTextRequest.Builder request =
                    TranslateTextRequest.newBuilder()
                            .setParent(parent.toString())
                            .setMimeType("text/plain")
                            .setSourceLanguageCode(qb.getSourceCode())
                            .setTargetLanguageCode(qb.getTargetCode())
                            .addContents(qb.getQueryText());
            String modelPath = getModelPath(projectCredentials, location);
            TranslateTextGlossaryConfig glossaryConfig = getGlossaryConfig(projectCredentials, location);
            if( modelPath != null ) {
                request.setModel(modelPath);
            }
            if( glossaryConfig != null ) {
                request.setGlossaryConfig(glossaryConfig);
            }
            TranslateTextResponse response = client.translateText(request.build());
            List<Translation> responseList = response.getTranslationsList();
            List<TranslationResponse> responses = new ArrayList<>();
            if (qb.getSourceCount() != responseList.size()) {
                LOG.error("Received {} translations for {} sources in query {}", responseList.size(),
                        qb.getSourceCount(), qb.getQueryText());
                throw new OkapiException("API returned incorrect number of translations (expected " +
                        qb.getSourceCount() + ", got " + responseList.size());
            }
            for (int i = 0; i < qb.getSourceCount(); i++) {
                responses.add(new TranslationResponse(qb.getSourceTexts().get(i), responseList.get(i).getTranslatedText()));
                System.out.println(responseList.get(i).getTranslatedText());
            }
            return responses;
        }
        catch ( Throwable e) {
            throw new OkapiException("Error creating service client: " + e.getMessage(), e);
        }
    }

    private TranslateTextGlossaryConfig getGlossaryConfig(String projectCredentials, String location) {
        if ( !Util.isEmpty(params.getGlossaryId()) ) {
            if (!location.equals("us-central1")) {
                LOG.error("Glossary location set to {}", location);
                throw new OkapiException("Glossary location cannot be " +
                        location + ", must be us-central1.");
            }
            GlossaryName glossaryName = GlossaryName.of(projectCredentials, location, params.getGlossaryId());
            TranslateTextGlossaryConfig glossaryConfig =
                    TranslateTextGlossaryConfig.newBuilder().setGlossary(glossaryName.toString()).build();
            return glossaryConfig;
        }
        return null;
    }

    private String getModelPath(String projectCredentials, String location) {
        if ( !Util.isEmpty(params.getModelId()) ) {
            String modelPath =
                    String.format("projects/%s/locations/%s/models/%s", projectCredentials, location, params.getModelId());
            return modelPath;
        }
        return null;
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T> TranslationResponse translateSingleSegment(GoogleQueryBuilder<T> qb, String sourceText)
            throws IOException, ParseException {
        LOG.debug("Using POST query for source '{}...' of length {}", sourceText.substring(0, 32), sourceText.length());
        URL url = new URL(qb.getQueryText());
        HttpURLConnection conn = (HttpURLConnection)url.openConnection();
        conn.setRequestMethod("POST");
        conn.setRequestProperty("Content-Type", "application/json");
        conn.setDoOutput(true);

        JSONObject json = new JSONObject();
        json.put("q", sourceText);
        try (OutputStreamWriter w = new OutputStreamWriter(conn.getOutputStream(), StandardCharsets.UTF_8)) {
            w.write(json.toJSONString());
        }
        GoogleResponseParser parser = new GoogleResponseParser();
        int code = conn.getResponseCode();
        if ( code == 200 ) {
            List<String> translatedTexts =
                    parser.parseResponse(new InputStreamReader(conn.getInputStream(), StandardCharsets.UTF_8));
            if (translatedTexts.size() != 1) {
                LOG.error("Received {} translations for {} sources in POST query {} with body '{}'", translatedTexts.size(),
                        1, qb.getQueryText(), sourceText);
                throw new OkapiException("API returned incorrect number of translations (expected 1, got " +
                        translatedTexts.size());
            }
            return new TranslationResponse(sourceText, translatedTexts.get(0));
        }
        else {
            String errorBody = StreamUtil.streamUtf8AsString(conn.getErrorStream());
            throw parser.parseError(code, errorBody, qb.toString());
        }
    }


}
