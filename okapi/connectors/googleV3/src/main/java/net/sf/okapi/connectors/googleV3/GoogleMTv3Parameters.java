/*===========================================================================
  Copyright (C) 2011-2017 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
  http://www.apache.org/licenses/LICENSE-2.0
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.connectors.googleV3;

import net.sf.okapi.common.ParametersDescription;
import net.sf.okapi.common.StringParameters;
import net.sf.okapi.common.uidescription.EditorDescription;
import net.sf.okapi.common.uidescription.IEditorDescriptionProvider;
import net.sf.okapi.common.uidescription.TextInputPart;

public class GoogleMTv3Parameters extends StringParameters implements IEditorDescriptionProvider {

	private static final String PROJECTID = "projectId";
	private static final String PROJECTNUMBER = "projectNumber";
	private static final String MODELID = "modelId";
	private static final String LOCATION = "location";
	private static final String GLOSSARYID = "glossaryId";
	private static final String RETRY_MS = "retryIntervalMs";
	private static final String RETRY_COUNT = "retryCount";
	private static final String FAILURES_BEFORE_ABORT = "failuresBeforeAbort";
	private static final String USE_PBMT = "usePBMT";
	private static final String inputUri = "gs://your-gcs-bucket/path/to/input/file.txt";
	private static final String outputUri = "gs://your-gcs-bucket/path/to/results/";

	public GoogleMTv3Parameters () {
	}


	public String getProjectNumber () {
		return getString(PROJECTNUMBER).trim();
	}

	public void setProjectNumber (String projectNumber) {
		if (projectNumber != null) {
			projectNumber = projectNumber.trim();
		}
		setString(PROJECTNUMBER, projectNumber);
	}

	public String getInputUri () {
		return getString(inputUri).trim();
	}

	public void setInputUri (String inputURI) {
		if (inputURI != null) {
			inputURI = inputURI.trim();
		}
		setString(inputUri, inputURI);
	}

	public String getOutputUri () {
		return getString(outputUri).trim();
	}

	public void setOutputUri (String outputURI) {
		if (outputURI != null) {
			outputURI = outputURI.trim();
		}
		setString(outputUri, outputURI);
	}

	public String getProjectId() {
		return getString(PROJECTID).trim();
	}

	public void setProjectId (String projectId) {
		if (projectId != null) {
			projectId = projectId.trim();
		}
		setString(PROJECTID, projectId);
	}

	public String getModelId() {
		return getString(MODELID).trim();
	}

	public void setModelId (String modelId) {
		if (modelId != null) {
			modelId = modelId.trim();
		}
		setString(MODELID, modelId);
	}

	public String getGlossaryId() {
		return getString(GLOSSARYID).trim();
	}

	public void setGlossaryid (String glossaryId) {
		if (glossaryId != null) {
			glossaryId = glossaryId.trim();
		}
		setString(MODELID, glossaryId);
	}

	public String getLocation () {
		return getString(LOCATION).trim();
	}

	public void setLocation (String location) {
		if (location != null) {
			location = location.trim();
		}
		setString(LOCATION, location);
	}

	public int getRetryIntervalMs () {
		return getInteger(RETRY_MS);
	}

	public void setRetryIntervalMs (int retryMs) {
		setInteger(RETRY_MS, retryMs);
	}

	public int getRetryCount () {
		return getInteger(RETRY_COUNT);
	}

	public void setRetryCount (int retryCount) {
		setInteger(RETRY_COUNT, retryCount);
	}

	public boolean getUsePBMT () {
		return getBoolean(USE_PBMT);
	}

	public void setUsePBMT (boolean usePBMT) {
		setBoolean(USE_PBMT, usePBMT);
	}

	public int getFailuresBeforeAbort () {
		return getInteger(FAILURES_BEFORE_ABORT);
	}

	// Use -1 for not aborting (backward compatible behavior)
	public void setFailuresBeforeAbort (int failuresBeforeAbort) {
		setInteger(FAILURES_BEFORE_ABORT, failuresBeforeAbort);
	}

	@Override
	public void reset () {
		super.reset();
		setProjectId("");
		setProjectNumber("");
		setUsePBMT(false);
		setModelId("");
		setGlossaryid("");
		setLocation("global");
		// The most likely error we will encounter is the rate limit of 100k
		// characters translated per 100 seconds.  We will retry every 10s
		// up to 10x, which is enough to flush the rate limit.
		setRetryIntervalMs(10 * 1000);
		setRetryCount(10);
		setFailuresBeforeAbort(-1);
	}

	@Override
	public ParametersDescription getParametersDescription () {
		ParametersDescription desc = new ParametersDescription(this);
		desc.add(PROJECTNUMBER,
				"Google Cloud project number",
				"The Google project number to identify the application/user");
		desc.add(PROJECTID,
				"Google Cloud project ID",
				"The Google project ID to identify the application/user");
		desc.add(USE_PBMT,
				"Use Phrase-Based MT",
				"Use the legacy PBMT system rather than Neural MT");
		desc.add(RETRY_COUNT,
				"Retry Count",
				"Number of retries to attempt before failing");
		desc.add(RETRY_MS,
				"Retry Interval (ms)",
				"Time to wait before retrying a failed query");
		desc.add(FAILURES_BEFORE_ABORT,
				"Failures before abort",
				"Number of times we let queries fail (after retries) before aborting the process");
		return desc;
	}

	@Override
	public EditorDescription createEditorDescription (ParametersDescription paramsDesc) {
		EditorDescription desc = new EditorDescription("Google Translate v3 Connector Settings", true, false);
		TextInputPart tip = desc.addTextInputPart(paramsDesc.get(PROJECTID));
		tip.setPassword(true);
		desc.addCheckboxPart(paramsDesc.get(USE_PBMT));
		desc.addTextInputPart(paramsDesc.get(RETRY_COUNT));
		desc.addTextInputPart(paramsDesc.get(RETRY_MS));
		desc.addTextInputPart(paramsDesc.get(FAILURES_BEFORE_ABORT));
		return desc;
	}

}