/*===========================================================================
  Copyright (C) 2011-2017 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
  http://www.apache.org/licenses/LICENSE-2.0
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.connectors.googleV3;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import com.google.api.core.ApiFuture;
import com.google.api.gax.longrunning.OperationFuture;
import com.google.cloud.translate.v3.*;
import com.google.cloud.translate.v3.DetectLanguageResponse;
import com.google.cloud.translate.v3.GetSupportedLanguagesRequest;
import com.google.cloud.translate.v3.Glossary;
import com.google.cloud.translate.v3.Glossary.LanguageCodesSet;
import com.google.cloud.translate.v3.GlossaryInputConfig;
import com.google.cloud.translate.v3.GlossaryName;
import com.google.cloud.translate.v3.SupportedLanguage;
import com.google.cloud.translate.v3beta1.SupportedLanguages;
import com.google.cloud.translate.v3.TranslateTextGlossaryConfig;
import com.google.cloud.translate.v3.TranslateTextRequest;
import com.google.cloud.translate.v3.TranslateTextResponse;
import com.google.cloud.translate.v3.TranslationServiceClient.ListGlossariesPagedResponse;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import com.google.cloud.translate.v3.BatchTranslateResponse;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.resource.ITextUnit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.sf.okapi.common.IParameters;
import net.sf.okapi.common.Util;
import net.sf.okapi.common.exceptions.OkapiException;
import net.sf.okapi.common.query.QueryResult;
import net.sf.okapi.common.resource.TextFragment;
import net.sf.okapi.lib.translation.BaseConnector;
import net.sf.okapi.lib.translation.QueryUtil;

public class GoogleMTv3Connector extends BaseConnector {
    private String base_url = " ";

    private final Logger LOG = LoggerFactory.getLogger(getClass());
    private GoogleMTv3Parameters params;
    private int failureCount;
    private QueryUtil util;
    private GoogleMTAPI api;


    public GoogleMTv3Connector () {
        params = new GoogleMTv3Parameters();
    }

    public GoogleMTv3Connector(GoogleMTAPI api) {
        params = new GoogleMTv3Parameters();
    }

    @Override
    public void setParameters (IParameters params) {
        this.params = (GoogleMTv3Parameters)params;
    }

    @Override
    public GoogleMTv3Parameters getParameters () {
        return params;
    }

    @Override
    public void close () {
        // Nothing to do
    }
    @Override
    public String getName () {
        return "Google-MTv3";
    }

    @Override
    public String getSettingsDisplay () {
        return "Server: ";
    }

    @Override
    public void open () {
        failureCount = 0;
    }

    @Override
    public int query (String plainText) {
        return _query(plainText, plainText, new TextQueryResultBuilder(params, getName(), getWeight()));
    }

    @Override
    public int query (TextFragment frag) {
        return _query(util.toCodedHTML(frag), frag, new FragmentQueryResultBuilder(params, getName(), getWeight()));
    }

    private void retryInterval(int retryCount, String operation) {
        LOG.info("{} - retry {} (waiting {} ms)", operation, retryCount, params.getRetryIntervalMs());
        try {
            Thread.sleep(params.getRetryIntervalMs());
        } catch (InterruptedException e) {
            throw new OkapiException("Interrupted while trying to contact Google MT");
        }
    }

    protected <T> int _query(String queryText, T originalText, QueryResultBuilder<T> qrBuilder) {
        api = new GoogleMTAPIImpl(params);
        current = -1;
        if (queryText.isEmpty()) return 0;
        // Check that we have some Key available
        if ( Util.isEmpty(params.getProjectId()) && Util.isEmpty(params.getProjectNumber())) {
            throw new OkapiException("You must have a Google API key or project ID to use this connector.");
        }

        List<QueryResult> queryResults = new ArrayList<>();
        GoogleQueryBuilder<T> qb = new GoogleQueryBuilder<>(queryText, getParameters(), srcCode, trgCode);
        qb.addQuery(queryText, originalText);
        List<TranslationResponse> responses = executeQuery(qb, qrBuilder);
        if (responses != null) {
            queryResults.addAll(qrBuilder.convertResponses(responses, originalText));
        }
        else {
            // Underlying call failed for some reason, probably a timeout
            LOG.error("Received no results for query {}", qb.getQueryText());
            // Return the source text as a dummy translation so that we can maintain the correct indexing
            queryResults.add(qrBuilder.createDummyResponse(originalText));
        }
        if (queryResults.size() > 0) {
            current = 0;
            result = queryResults.iterator().next();
            return 1;
        }
        throw new OkapiException("Could not retrieve results from Google after " +
                params.getRetryCount() + " attempts.");
    }



    @Override
    public List<List<QueryResult>> batchQueryText(List<String> plainTexts) {
        return _batchQuery(plainTexts, plainTexts, new TextQueryResultBuilder(params, getName(), getWeight()));
    }

    @Override
    public List<List<QueryResult>> batchQuery (List<TextFragment> fragments) {
        return _batchQuery(util.toCodedHTML(fragments), fragments,
                new FragmentQueryResultBuilder(params, getName(), getWeight()));
    }

    protected <T> List<List<QueryResult>> _batchQuery(List<String> texts, List<T> originalTexts,
                                                      QueryResultBuilder<T> qrBuilder) {
        GoogleQueryBuilder<T> qb = new GoogleQueryBuilder<>(base_url, params, srcCode, trgCode);
        boolean processBatch = false;
        if (texts.isEmpty()) return null;
        if (Util.isEmpty(params.getProjectId()) && Util.isEmpty(params.getProjectNumber())) {
            throw new OkapiException("You must have a project ID or project Number to use this connector.");
        }
        int batchCharCount = 0;
        List<List<QueryResult>> results = new ArrayList<>(texts.size());
        List<String> textsToQuery = new ArrayList<>();
        List<T> originalTextsToQuery = new ArrayList<>();
        for (int end = 0; end < texts.size(); end++) {
            int charCount = texts.get(end).trim().length();
            // Process a single batch that is small enough to be handled in a single API call.
            //If text files exceed 1000, or number of UniCode points is beyond 1 million,
            //multiple batches must be used
            if (textsToQuery.size() >= api.maxTextCount || batchCharCount >= api.maxCharCount) {
                processBatch = true;
            }
            if (processBatch) {
                results.addAll(flushQuery(qb, qrBuilder));
                batchCharCount = 0;
                textsToQuery.clear();
                originalTextsToQuery.clear();
                end--; // Do this one again
            }
            else {
                qb.addQuery(texts.get(end).trim(), originalTexts.get(end));
                batchCharCount += charCount;
                textsToQuery.add(texts.get(end).trim());
                originalTextsToQuery.add(originalTexts.get(end));
            }
        }
        if (textsToQuery.size() > 0) {
            results.addAll(flushQuery(qb, qrBuilder));
            return results;
        }
        LOG.error("Failed to batch translate with Google v3  Translator ({} tries).",
                params.getRetryCount());
        throw new OkapiException("Could not retrieve results from Google v3 after " + params.getRetryCount() + " attempts.");
    }

    protected <T> List<List<QueryResult>> flushQuery(GoogleQueryBuilder<T> qb, QueryResultBuilder<T> qrBuilder) {
        List<List<QueryResult>> queryResults = new ArrayList<>();
        if (qb.getSourceCount() > 0) {
            LOG.debug("Flushing batch query of length {}, '{}'", qb.getQuery().length(), qb.getQuery());
            List<TranslationResponse> batchResponses = executeQuery(qb, qrBuilder);
            System.out.println(batchResponses.size());
            if(batchResponses == null) System.out.println("hooo");
            if (batchResponses != null) {
                for (int j = 0; j < batchResponses.size(); j++) {
                    System.out.println("in flush");
                    System.out.println(qb.getSources().get(j));
                    queryResults.add(qrBuilder.convertResponses(
                            Collections.singletonList(batchResponses.get(j)), qb.getSources().get(j)));
                }
            }
            else {
                // Underlying call failed for some reason, probably a timeout
                LOG.error("Received no results for query {}", qb.getQuery());
                // Return the source text as a dummy translation so that we can maintain the correct indexing
                for (T source : qb.getSources()) {
                    queryResults.add(Collections.singletonList(qrBuilder.createDummyResponse(source)));
                }
            }
            qb.reset();
        }
        return queryResults;
    }

    protected <T> TranslationResponse executeSingleSegmentQuery(GoogleQueryBuilder<T> qb, String sourceText) {
        try {
            for (int tries = 0; tries < params.getRetryCount(); tries++) {
                try {
                    return api.translateSingleSegment(qb, sourceText);
                }
                catch (GoogleMTErrorException e) {
                    LOG.error("Error {} - {} for query {}", e.getCode(), e.getMessage(), e.getQuery());
                }
                retryInterval(tries + 1, "_batchQuery");
            }
        }
        catch ( Throwable e) {
            throw new OkapiException("Error querying the MT server: " + e.getMessage(), e);
        }
        // All retries have failed
        if (( params.getFailuresBeforeAbort() > -1 ) && ( ++failureCount > params.getFailuresBeforeAbort() )) {
        	throw new OkapiException("Too many retry failures while querying the MT server.");
        }
        return null;
    }
    protected <T> List<TranslationResponse> executeQuery(GoogleQueryBuilder<T> qb, QueryResultBuilder<T> qrBuilder) {
        try {
            for (int tries = 0; tries < params.getRetryCount(); tries++) {
                try {
                    System.out.println(qb);
                    List<TranslationResponse> test= api.translate(qb);
                    System.out.println(test.size());
                    return test;
                }
                catch (GoogleMTErrorException e) {
                    LOG.error("Error {} - {} for query {}", e.getCode(), e.getMessage(), e.getQuery());
                }
                retryInterval(tries + 1, "_batchQuery");
            }
        }
        catch ( Throwable e) {
            throw new OkapiException("Error querying the MT server: " + e.getMessage(), e);
        }
        // All retries have failed
        if (( params.getFailuresBeforeAbort() > -1 ) && ( ++failureCount > params.getFailuresBeforeAbort() )) {
            throw new OkapiException("Too many retry failures while querying the MT server.");
        }
        return null;
    }

    public List<LocaleId> getSupportedLanguages() {
        try {
            for (int tries = 0; tries < params.getRetryCount(); tries++) {
                List<String> codes = api.getLanguages();
                if (codes != null) {
                    List<LocaleId> locales = new ArrayList<>();
                    for (String code : codes) {
                        locales.add(convertGoogleLanguageCode(code));
                    }
                    return locales;
                }
                retryInterval(tries + 1, "getSupportedLanguages");
            }
        }
        catch ( Throwable e) {
            throw new OkapiException("Error querying the MT server: " + e.getMessage(), e);
        }
        throw new OkapiException("Could not retrieve language list from Google after " +
                                 params.getRetryCount() + " attempts.");
    }

    protected LocaleId convertGoogleLanguageCode(String lang) {
        return LocaleId.fromBCP47(lang);
    }
	@Override
	public void leverage (ITextUnit tu) {
		leverageUsingBatchQuery(tu);
	}
	@Override
	public void batchLeverage (List<ITextUnit> tuList) {
		batchLeverageUsingBatchQuery(tuList);
	}
	@Override
	protected String toInternalCode (LocaleId locale) {
		String code = locale.toBCP47();
		if ( !code.startsWith("zh") && ( code.length() > 2 )) {
			code = code.substring(0, 2);
		}
		return code;
	}

	//Function is used for translating files stored in Cloud Storage Buckets using
    //Batch Translation Requests feature in v3
    //More info at https://cloud.google.com/translate/docs/advanced/batch-translation
    public static BatchTranslateResponse batchTranslateText(
            String projectId,
            String sourceLanguage,
            String targetLanguage,
            String inputUri,
            String outputUri,
            String glossaryId,
            String modelId)
            throws IOException, ExecutionException, InterruptedException, TimeoutException {

        // Initialize client that will be used to send requests. This client only needs to be created
        // once, and can be reused for multiple requests.
        try (TranslationServiceClient client = TranslationServiceClient.create()) {
            // Supported Locations: `us-central1`
            String location = "us-central1";
            LocationName parent = LocationName.of(projectId, location);

            GcsSource gcsSource = GcsSource.newBuilder().setInputUri(inputUri).build();
            // Supported Mime Types: https://cloud.google.com/translate/docs/supported-formats
            InputConfig inputConfig =
                    InputConfig.newBuilder().setGcsSource(gcsSource).build();

            GcsDestination gcsDestination =
                    GcsDestination.newBuilder().setOutputUriPrefix(outputUri).build();
            OutputConfig outputConfig =
                    OutputConfig.newBuilder().setGcsDestination(gcsDestination).build();

            BatchTranslateTextRequest.Builder request =
                    BatchTranslateTextRequest.newBuilder()
                            .setParent(parent.toString())
                            .setSourceLanguageCode(sourceLanguage)
                            .addTargetLanguageCodes(targetLanguage)
                            .addInputConfigs(inputConfig)
                            .setOutputConfig(outputConfig);

            // Configure the glossary used in the request
            if(glossaryId.trim().length() != 0) {
                GlossaryName glossaryName = GlossaryName.of(projectId, location, glossaryId);
                TranslateTextGlossaryConfig glossaryConfig =
                        TranslateTextGlossaryConfig.newBuilder().setGlossary(glossaryName.toString()).build();
                request.putGlossaries(targetLanguage, glossaryConfig);
            }
            // Configure the model used in the request
            if(modelId.trim().length() != 0) {
                String modelPath =
                        String.format("projects/%s/locations/%s/models/%s", projectId, location, modelId);
                request.putModels(targetLanguage, modelPath);
            }


            OperationFuture<BatchTranslateResponse, BatchTranslateMetadata> future =
                    client.batchTranslateTextAsync(request.build());

            BatchTranslateResponse response = future.get(180, TimeUnit.SECONDS);
            return response;
        }
    }

}