package net.sf.okapi.connectors.googleV3;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import com.google.cloud.translate.v3.BatchTranslateResponse;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.resource.TextFragment;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import net.sf.okapi.common.exceptions.OkapiException;
import net.sf.okapi.common.query.QueryResult;

@RunWith(MockitoJUnitRunner.class)
public class TestGoogleMTv3Connector {

    @Mock
    private GoogleMTAPI api;
    private GoogleMTAPIImpl apiImpl;

    private GoogleMTv3Connector connector;
    private int count = 0;

    @Before
    public void setup() {
        connector = new GoogleMTv3Connector(api);
        connector.getParameters().setRetryIntervalMs(0);
        connector.getParameters().setProjectId("1234");
    }

    @Test
    public void testQuery() throws Exception {
        GoogleMTv3Parameters params = new GoogleMTv3Parameters();
        params.reset();
        params.setProjectId("okapi-connector-v3");
        connector.setLanguages(LocaleId.ENGLISH, LocaleId.FRENCH);
        int result = connector.query(testString());
    }
    private String testString() {
        return "Hello world";
    }

    @Test
    public void testBatchQueryLocal() throws Exception {
        GoogleMTAPI api = Mockito.mock(GoogleMTAPI.class);
        List<TranslationResponse> responses = new ArrayList<>();
        //query
        int numTexts = 102;
        List<String> sourcetexts = new ArrayList<>(numTexts);
        sourcetexts.add("Hello World");
        responses.add(new TranslationResponse("Hello World", "Translation 1"));
        Mockito.when(api.translate(any(GoogleQueryBuilder.class))).thenReturn(responses);
        connector.open();
        List<List<QueryResult>> response = connector.batchQueryText(sourcetexts);
        assertEquals(1, response);
        assertTrue(connector.hasNext());
        QueryResult qr = connector.next();
        assertEquals(new TextFragment("Translation 1"), qr.target);
    }

    @Test
    public void testBatchQueryBucket() throws Exception {
        GoogleMTv3Parameters params = new GoogleMTv3Parameters();
        params.reset();
        params.setProjectId("okapi-connector-v3");
        params.setInputUri("gs://inputv3test/*");
        params.setOutputUri("gs://outputv3test/");
        connector.batchTranslateText
                (params.getProjectId(), "en", "es", params.getInputUri(), params.getOutputUri(), "", "");
    }

    @Test
    public void testManyPieces() {
        // Run batch query
        connector.open();
        connector.setLanguages(LocaleId.ENGLISH, LocaleId.FRENCH);

        int numTexts = 102;
        List<String> texts = new ArrayList<>(numTexts);
        for(int i=0; i<numTexts; ++i) {
            texts.add(String.format("%d sheep", i));
        }
        System.out.println(texts);
        List<List<QueryResult>> translatedTexts = connector.batchQueryText(texts);

        assertEquals(numTexts, translatedTexts.size());
        assertEquals(1, translatedTexts.get(0).size());
        assertEquals("0 ovejas", translatedTexts.get(0).get(0).target.getText());
        assertEquals("101 ovejas", translatedTexts.get(numTexts-1).get(0).target.getText());

        connector.close();
    }


    @Test
    public void testBatchQueryRetry() throws Exception {
        when(api.translate(any(GoogleQueryBuilder.class))).thenAnswer(new MTAnswer());
        List<String> inputs = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            inputs.add(dummyString(i));
        }
        List<List<QueryResult>> results = connector.batchQueryText(inputs);
        assertEquals(10, results.size());
        for (int i = 0; i < results.size(); i++) {
            List<QueryResult> qrs = results.get(i);
            assertEquals(1, qrs.size());
            assertEquals(translation(inputs.get(i)), qrs.get(0).target.toString());
        }
    }

    @Test
    public void testQueryRetry() throws Exception {
        when(api.translate(any(GoogleQueryBuilder.class))).thenAnswer(new MTAnswer());
        String input = dummyString(1);
        int i = connector.query(input);
        assertEquals(1, i);
        assertEquals(translation(input), connector.next().target.toString());
    }

    @Test
    public void testQueryRetryWithAbort() throws Exception {
        try ( GoogleMTv3Connector conn = new GoogleMTv3Connector(api) ) {
            conn.getParameters().setRetryIntervalMs(10);
            conn.getParameters().setRetryCount(1);
            conn.getParameters().setFailuresBeforeAbort(1);

            when(api.translate(any(GoogleQueryBuilder.class))).thenAnswer(new MTAnswer());
            List<String> inputs = new ArrayList<>();
            for (int i = 0; i < 10; i++) {
                inputs.add(dummyString(i));
            }
            String msg = null;
            try {
            	conn.batchQueryText(inputs);
            }
            catch ( OkapiException e ) {
            	msg = e.getMessage();
            }
            assertTrue(msg.startsWith("Too many retry failures"));
        }
    }

    class MTAnswer implements Answer<List> {
        @Override
        public List answer(InvocationOnMock invocation) throws Throwable {
            // This fails 50% of the time
            if (count++ % 2 == 0) {
                throw new GoogleMTErrorException(403, "User limit exceeded", "domain", "reason", "query");
            }
            else {
                GoogleQueryBuilder<GoogleQueryBuilder> qb = invocation.getArgument(0);
                List<TranslationResponse> responses = new ArrayList<>();
                for (String s : qb.getSourceTexts()) {
                    responses.add(new TranslationResponse(s, translation(s)));
                }
                return responses;
            }
        }
    }
    private String dummyString(int testNum) {
        StringBuilder sb = new StringBuilder();
        for (int j = 0; j < 1800; j++) {
            sb.append('x');
        }
        String input = "Test " + testNum + ": " + sb.toString();
        return input;
    }
    private String translation(String s) {
        return new StringBuilder(s).reverse().toString();
    }
}