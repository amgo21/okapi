package net.sf.okapi.connectors.googleV3;
import com.google.cloud.translate.v3.BatchTranslateResponse;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.resource.TextFragment;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;
import static org.junit.Assert.assertEquals;

import net.sf.okapi.common.exceptions.OkapiException;
import net.sf.okapi.common.query.QueryResult;

import java.util.ArrayList;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class TestGoogleMTv3Batch {

    @Mock
    private GoogleMTAPI api;
    private GoogleMTAPIImpl apiImpl;
    private GoogleMTv3Parameters params;
    private GoogleMTv3Connector connector;
    private int count = 0;

    @Before
    public void setup() {
        connector = new GoogleMTv3Connector(api);
        params = connector.getParameters();
        connector.getParameters().setRetryIntervalMs(0);
        connector.getParameters().setProjectId("okapi-connector-v3");
        connector.getParameters().setOutputUri("gs://outputv3test");
        connector.getParameters().setInputUri("gs://inputv3test");
    }




}

